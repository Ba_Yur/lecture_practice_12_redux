import 'package:flutter/foundation.dart';

class WeatherDto {
  final String id;
  final String windSpeed;
  final String description;
  final String temp;

  WeatherDto({
    @required this.id,
    @required this.windSpeed,
    @required this.description,
    @required this.temp,
  });

  factory WeatherDto.fromJson(Map<String, dynamic> json) {
    print('dto');
    return WeatherDto(
      id: json['weather'][0]['id'].toString(),
      windSpeed: json['wind']['speed'].toString(),
      description: json['weather'][0]['description'].toString(),
      temp: json['main']['temp'].toString(),
    );
  }
}
