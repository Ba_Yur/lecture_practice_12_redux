import 'package:lecture_practice_12_redux/dto/weather_dto.dart';
import 'package:lecture_practice_12_redux/repositories/weather_info_repository.dart';
import 'package:lecture_practice_12_redux/store/app/app_state.dart';
import 'package:lecture_practice_12_redux/store/pages/weather_page_state/weather_page_actions.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

class WeatherEpics {
  static final indexEpic = combineEpics<AppState>([
    setWeatherEpic,
  ]);

  static Stream<dynamic> setWeatherEpic(Stream<dynamic> actions, EpicStore<AppState> store) {
    print('aaaaaa');
    return actions.whereType<GetWeatherAction>().switchMap(
      (action) {
        print('asd');
        return Stream.fromFuture(
          WeatherInfoRepository.instance.getWeather().then(
            (WeatherDto weather) {
              print('weather');
              return SaveWeatherAction(
                weather: weather,
              );
            },
          ),
        );
      },
    );
  }
}
