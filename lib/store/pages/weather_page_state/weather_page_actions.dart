import 'package:flutter/material.dart';
import 'package:lecture_practice_12_redux/dto/weather_dto.dart';

class GetWeatherAction {
 GetWeatherAction();
}

class SaveWeatherAction{
  final WeatherDto weather;

  SaveWeatherAction({@required this.weather});
}
