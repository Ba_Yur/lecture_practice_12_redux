import 'dart:collection';

import 'package:lecture_practice_12_redux/dto/weather_dto.dart';
import 'package:lecture_practice_12_redux/store/app/reducer.dart';
import 'package:lecture_practice_12_redux/store/pages/weather_page_state/weather_page_actions.dart';

class WeatherState {
  final String id;
  final String windSpeed;
  final String description;
  final String temp;

  WeatherState(
    this.id,
    this.windSpeed,
    this.description,
    this.temp,
  );

  factory WeatherState.initial() => WeatherState('', '', '', '');

  WeatherState copyWith({String id, String windSpeed, String description, String temp}) {
    return WeatherState(
      id ?? this.id,
      windSpeed ?? this.windSpeed,
      description ?? this.description,
      temp ?? this.temp,
    );
  }

  WeatherState reducer(dynamic action) {
    return Reducer<WeatherState>(
      actions: HashMap.from({
        SaveWeatherAction: (dynamic action) =>
            saveWeather((action as SaveWeatherAction).weather),
      }),
    ).updateState(action, this);
  }

  WeatherState saveWeather(WeatherDto weatherDto) {
    print('save');
    return copyWith(
      temp: weatherDto.temp,
      description: weatherDto.description,
      windSpeed: weatherDto.windSpeed,
      id: weatherDto.id,
    );
  }
}
