

import 'package:lecture_practice_12_redux/dto/weather_dto.dart';
import 'package:lecture_practice_12_redux/store/app/app_state.dart';
import 'package:lecture_practice_12_redux/store/pages/weather_page_state/weather_page_actions.dart';
import 'package:redux/redux.dart';


class WeatherSelector {
  static void Function() getWeatherFunction(Store<AppState> store) {
    print('getting weather');
    return () => store.dispatch(GetWeatherAction());
  }

  static String getCurrentWeatherWind(Store<AppState> store) {
    print ('store.state.weatherPageState.windSpeed');
    return store.state.weatherPageState.windSpeed;
  }

  static String getCurrentWeatherDescription(Store<AppState> store) {
    print ('store.state.weatherPageState.description');
    return store.state.weatherPageState.description;
  }
}