import 'dart:collection';

import 'package:flutter/material.dart';

import './some_page_actions.dart';
import '../../app/reducer.dart';

class SomePageState {
  final int counter;
  final bool isGray;
  final Color bgColor;

  SomePageState({
    this.counter,
    this.isGray,
    this.bgColor,
  });

  factory SomePageState.initial() {
    return SomePageState(
      counter: 0,
      isGray: true,
    );
  }

  SomePageState copyWith({
    bool isGray,
    int counter,
    Color bgColor,
  }) {
    return SomePageState(
      counter: counter ?? this.counter,
      isGray: isGray ?? this.isGray,
      bgColor: bgColor ?? this.bgColor,
    );
  }

  SomePageState reducer(dynamic action) {
    return Reducer<SomePageState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => _incrementCounter(),
        DecrementAction: (dynamic action) => _decrementCounter(),
        ClearAction: (dynamic action) => _clearCounter(),
        ChangeColorAction: (dynamic action) => _changeColor(),
        ChangeBgColorAction: (dynamic action) => _changeBgColor(action.color),
      }),
    ).updateState(action, this);
  }

  SomePageState _incrementCounter() {
    return copyWith(counter: counter + 1);
  }

  SomePageState _decrementCounter() {
    return copyWith(counter: counter - 1);
  }

  SomePageState _clearCounter() {
    return copyWith(counter: 0);
  }

  SomePageState _changeColor() {
    return copyWith(isGray: !isGray);
  }

  SomePageState _changeBgColor(Color color) {
    return copyWith(bgColor: color);
  }
}
