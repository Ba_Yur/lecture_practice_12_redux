

import 'dart:ui';

import 'package:lecture_practice_12_redux/store/app/app_state.dart';
import 'package:lecture_practice_12_redux/store/pages/some_page_state/some_page_actions.dart';
import 'package:redux/redux.dart';

class SomePageSelector {

  static int getCounter(Store<AppState> store) {
    return store.state.somePageState.counter;
  }
  static Color getBgColor(Store<AppState> store) {
    return store.state.somePageState.bgColor;
  }

  static bool getColor(Store<AppState> store) {
    return store.state.somePageState.isGray;
  }

  static void Function() incrementCounterFunction(Store<AppState> store) {
    return () {
      store.dispatch(IncrementAction());
    };
  }


  static void Function() decrementCounterFunction(Store<AppState> store) {
    return () {
      store.dispatch(DecrementAction());
    };
  }

  static void Function() clearCounterFunction(Store<AppState> store) {
    return () {
      store.dispatch(ClearAction());
    };
  }

  static void Function() changeColorFunction(Store<AppState> store) {
    return () {
      store.dispatch(ChangeColorAction());
    };
  }

  static void Function(Color color) changeBgColorFunction(Store<AppState> store) {
    return (color) {
      store.dispatch(ChangeBgColorAction(color: color));
    };
  }

}