import 'package:flutter/cupertino.dart';
import 'package:lecture_practice_12_redux/store/pages/some_page_state/some_page_state.dart';
import 'package:lecture_practice_12_redux/store/pages/weather_page_state/weather_page_epics.dart';
import 'package:lecture_practice_12_redux/store/pages/weather_page_state/weather_page_state.dart';
import 'package:redux_epics/redux_epics.dart';

class AppState {
  final SomePageState somePageState;
  final WeatherState weatherPageState;

  AppState({
    @required this.somePageState,
    @required this.weatherPageState,
  });

  factory AppState.initial() {
    return AppState(
      somePageState: SomePageState.initial(),
      weatherPageState: WeatherState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      somePageState: state.somePageState.reducer(action),
      weatherPageState: state.weatherPageState.reducer(action),
    );
  }

  static final getAppEpic = combineEpics<AppState>([
    WeatherEpics.indexEpic,
  ]);
}
