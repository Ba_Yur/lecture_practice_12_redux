import 'package:flutter/cupertino.dart';
import 'package:lecture_practice_12_redux/store/pages/some_page_state/some_page_selectors.dart';
import 'package:lecture_practice_12_redux/store/pages/weather_page_state/weather_page_selectors.dart';
import 'package:redux/redux.dart';

import 'app/app_state.dart';

class SomePageViewModel {
  final bool isGray;
  final int counter;
  final Color bgColor;
  final void Function() increment;
  final void Function() decrement;
  final void Function() clear;
  final void Function() changeColor;
  final void Function(Color color) changeBgColor;

  final String wind;
  final void Function() getWind;
  final String description;


  SomePageViewModel({
    @required this.isGray,
    @required this.counter,
    @required this.bgColor,
    @required this.increment,
    @required this.decrement,
    @required this.clear,
    @required this.changeColor,
    @required this.changeBgColor,

    @required this.wind,
    @required this.getWind,
    @required this.description,


  });

  static SomePageViewModel fromStore(Store<AppState> store) {
    return SomePageViewModel(
      isGray: SomePageSelector.getColor(store),
      counter: SomePageSelector.getCounter(store),
      bgColor: SomePageSelector.getBgColor(store),
      increment: SomePageSelector.incrementCounterFunction(store),
      decrement: SomePageSelector.decrementCounterFunction(store),
      clear: SomePageSelector.clearCounterFunction(store),
      changeColor: SomePageSelector.changeColorFunction(store),
      changeBgColor: SomePageSelector.changeBgColorFunction(store),
      wind: WeatherSelector.getCurrentWeatherWind(store),
      getWind: WeatherSelector.getWeatherFunction(store),
        description : WeatherSelector.getCurrentWeatherDescription(store)
    );
  }
}
