
import 'package:lecture_practice_12_redux/dto/weather_dto.dart';
import 'package:lecture_practice_12_redux/services/weather_service.dart';

class WeatherInfoRepository {

  WeatherInfoRepository._privateConstructor();

  static final WeatherInfoRepository instance = WeatherInfoRepository._privateConstructor();

  Future<WeatherDto> getWeather() async {
    Map<String, dynamic> responseData = await WeatherService.instance.getWeather();
    print('repo');
    WeatherDto weather =  WeatherDto.fromJson(responseData);
    return weather;
  }
}