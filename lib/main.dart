import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:lecture_practice_12_redux/repositories/weather_info_repository.dart';
import 'package:lecture_practice_12_redux/store/app/app_state.dart';
import 'package:lecture_practice_12_redux/store/some_page_viewmodel.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';

void main() {
  Store store = Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
    middleware: [
      EpicMiddleware(AppState.getAppEpic),
    ]
  );
  runApp(MyApp(
    store: store,
  ));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  const MyApp({this.store});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, SomePageViewModel>(
          converter: SomePageViewModel.fromStore,
          builder: (BuildContext context, SomePageViewModel vm) {
            return Scaffold(
              backgroundColor: vm.bgColor,
              appBar: AppBar(
                backgroundColor: vm.isGray ? Colors.black38 : Colors.orange,
                title: Text('Counter Redux'),
              ),
              drawer: Drawer(
                child: Container(
                  height: 500,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Switch(
                        value: vm.isGray,
                        onChanged: (value) {
                          vm.changeColor();
                          Navigator.pop(context);
                        },
                      ),
                      ElevatedButton(
                        onPressed: () {
                          vm.clear();
                          Navigator.pop(context);
                        },
                        child: Text('clear counter'),
                      ),
                      Container(
                        height: 300,
                        child: ListView(
                          itemExtent: 70.0,
                          children: [
                            InkWell(
                              child: Container(
                                height: 50.0,
                                color: Colors.red,
                              ),
                              onTap: () {
                                vm.changeBgColor(Colors.red);
                              },
                            ),
                            InkWell(
                              child: Container(
                                height: 50.0,
                                color: Colors.blue,
                              ),
                              onTap: () {
                                vm.changeBgColor(Colors.blue);
                              },
                            ),
                            InkWell(
                              child: Container(
                                height: 50.0,
                                color: Colors.green,
                              ),
                              onTap: () {
                                vm.changeBgColor(Colors.green);
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text('Wind speed in Odessa-city: ${vm.wind}',style: TextStyle(
                    fontSize: 20
                  ),),
                  Text('Weather description: ${vm.description}',style: TextStyle(
                      fontSize: 20
                  ),),
                  Text(
                    vm.counter.toString(),
                    style: TextStyle(fontSize: 150),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      ElevatedButton(
                        onPressed: vm.decrement,
                        child: Text('Decrement'),
                      ),
                      ElevatedButton(
                        onPressed: vm.getWind,
                        child: Text('Get weather'),
                      ),
                      ElevatedButton(
                        onPressed: () {
                          WeatherInfoRepository.instance.getWeather();
                          vm.increment();
                        },
                        child: Text('Increment'),
                      ),
                    ],
                  )
                ],
              ),
            );
          });

  }
}
